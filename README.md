# The Traveling Salesman Problem

## What is it?
The traveling salesman problem (TSP) is a well-known problem in computer science and operations research that involves finding the shortest possible route that visits a given set of coordinates and returns to the starting coordinate.

## How to use it?
TBC