export default class SalesmanSolver {
    /** @param {HTMLCanvasElement} canvasElement */
    constructor(canvasElement) {
        this.canvas = canvasElement;
        this.context = canvasElement.getContext("2d");
        this.locationPoints = [];
        this.generateNum = 50000;
        this.backgroundColor = "#2d2d2d";
        this.pointColor = "#ffffff";

        this.lineDrawn = false;
        this._lineLength = 0;
        this.lineLengthObservers = [];
        
        this.init();
        window.addEventListener("resize", this.init);
        
    }
    
    init = () => {
        this.reset();
        this.lineDrawn = false;
        this.lineLength = 0;
        this.canvas.width = this.canvas.parentElement.clientWidth;
        this.canvas.height = this.canvas.parentElement.clientHeight;
        this.context.fillStyle = this.backgroundColor;
        this.context.fillRect(0, 0, this.canvas.width, this.canvas.height);
        this.locationPoints = [];
        this.context.fillStyle = this.pointColor;
        this.pointSize = 5;
        this.startClickListener();
    }

    reset = () => {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

    get lineLength() {
        return this._lineLength;
    }

    set lineLength(newLength) {
        this._lineLength = newLength;
        this.lineLengthObservers.forEach(observer => observer(newLength));
    }

    addPoint = (x, y) => {
        this.locationPoints.push({ x, y });
        this.drawPoint(x, y);   
    }

    drawPoint = (x, y) => {
        if(this.pointSize === 1) {
            this.context.fillRect(x, y, 1, 1);
            return;
        }
        this.context.beginPath();
        this.context.arc(x, y, this.pointSize, 0, 2 * Math.PI);
        this.context.fill();
    }

    drawLine = (x1, y1, x2, y2) => {
        this.context.strokeStyle = this.pointColor;
        this.context.beginPath();
        this.context.moveTo(x1, y1);
        this.context.lineTo(x2, y2);
        this.context.stroke();
    }

    disatanceBetweenPoints = (x1, y1, x2, y2) => {
        return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    }

    getPosition = (event) => {
        const rect = this.canvas.getBoundingClientRect();
        const x = event.clientX - rect.left;
        const y = event.clientY - rect.top;

        this.addPoint(Math.round(x), Math.round(y));
    }

    startClickListener = () => {
        this.removeClickListener();

        this.canvas.addEventListener('click', this.getPosition);
    }

    removeClickListener = () => {
        this.canvas.removeEventListener('click', this.getPosition);
    }

    logDistance = (distance) => {
        console.log(`Distance: ${Math.round(distance)}px`);
    }

    generatePath = async () => {
        if(this.locationPoints.length < 3) {
            alert("Please add at least 3 points");
            return;
        }

        if(this.lineDrawn) return;

        this.removeClickListener();
        // this.lineLength = await this.generatePathInOrder();
        this.lineLength = Math.round(await this.generatePathNextShortest());

        this.lineDrawn = true;
    }
    
    generatePathInOrder = async () => {
        let distance = 0;
        // Draw a line between each point
        this.locationPoints.forEach((point, index) => {
            if(index === this.locationPoints.length - 1) {
                this.drawLine(point.x, point.y, this.locationPoints[0].x, this.locationPoints[0].y);
                distance += this.disatanceBetweenPoints(point.x, point.y, this.locationPoints[0].x, this.locationPoints[0].y);
                return;
            }
            this.drawLine(point.x, point.y, this.locationPoints[index + 1].x, this.locationPoints[index + 1].y);
            distance += this.disatanceBetweenPoints(point.x, point.y, this.locationPoints[index + 1].x, this.locationPoints[index + 1].y);
        });
    
        return distance;
    }

    generatePathNextShortest = async () => {
        let distance = 0;
        const sortedLocations = [this.locationPoints[0]];

        while(sortedLocations.length < this.locationPoints.length) {
            let shortestDistance = Infinity;
            let shortestIndex = 0;
            let latestPoint = sortedLocations[sortedLocations.length - 1];

            this.locationPoints.forEach((point2, index2) => {
                if(sortedLocations.includes(point2)) return;
                const distance = this.disatanceBetweenPoints(latestPoint.x, latestPoint.y, point2.x, point2.y);
                if(distance < shortestDistance) {
                    shortestDistance = distance;
                    shortestIndex = index2;
                }
            });
            sortedLocations.push(this.locationPoints[shortestIndex]);
        }



        // // Sort locations by distance from the first point
        // const sortedLocations = this.locationPoints.sort((a, b) => {
        //     return this.disatanceBetweenPoints(a.x, a.y, this.locationPoints[0].x, this.locationPoints[0].y) - this.disatanceBetweenPoints(b.x, b.y, this.locationPoints[0].x, this.locationPoints[0].y);
        // });

        // Draw a line between each point
        sortedLocations.forEach((point, index) => {
            if(index === sortedLocations.length - 1) {
                this.drawLine(point.x, point.y, sortedLocations[0].x, sortedLocations[0].y);
                distance += this.disatanceBetweenPoints(point.x, point.y, sortedLocations[0].x, sortedLocations[0].y);
                return;
            }
            this.drawLine(point.x, point.y, sortedLocations[index + 1].x, sortedLocations[index + 1].y);
            distance += this.disatanceBetweenPoints(point.x, point.y, sortedLocations[index + 1].x, sortedLocations[index + 1].y);
        });

        return distance;
    }
}